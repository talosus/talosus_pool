Talosus-nodejs-pool
======================

Forked from https://github.com/dvandal/cryptonote-nodejs-pool 


#### Table of Contents
* [About](#about)
* [Install](#install)
* [Prerequisites](#prerequisites)
* [Pools](#pools)
* [Donations](#donations)
* [Credits](#credits)
* [License](#license)


About
===

This repo contains everything you need to start a Talosus Pool. 

The instructions are granny proof I have tested.

Prerequisites
===
You'll need a server running Ubuntu 16.04.

https://www.hetzner.com We use in the EU 

(Login and go to servers select "Add Server". 
Choose a location and select 16.04 from Ubuntu dropdown. 
Either CX21 or CX31 will have enough resources to run a small to medium pool.
Click "Create & Buy Now".
You will be emailed the root password.
Follow the Installation guide.)

////TODO -- Add other hosts///


Install
===
This guide is written with Ubuntu 16.04 LTS. Others may work but they may not be the same as this.

(Download Node.js)
```bash
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash
```
(Download Redis Server)
```bash
sudo add-apt-repository ppa:chris-lea/redis-server
```
(Update package list)
```bash
sudo apt-get update
```
(Install dependencies. Press Y then Enter when asked. Some will not be needed but it will cut down on support requests)
```bash
sudo apt update && sudo apt install build-essential cmake pkg-config libboost-all-dev libssl-dev libzmq3-dev libunbound-dev libsodium-dev libunwind8-dev liblzma-dev libreadline6-dev libldns-dev libexpat1-dev doxygen graphviz libpgm-dev libpcsclite1 libssl-dev redis-server nodejs
```
(Create a user without login or password. Replace <YourUserName> with anything you want. Press Enter for all option then Y when asked.)
```bash
sudo adduser --disabled-password --disabled-login <YourUserName>    
```
(Swap from Root to your new user account. Replace <YourUserName> with what you chose in the previous step.)    
```bash
sudo su - <YourUserName>
```
(Download the pool files)
```bash
git clone https://Wasntafairfight@bitbucket.org/talosus/talosus_pool.git
```
(Move to the talosus_pool directory)
```bash
cd talosus_pool
```
(Move to the backend directory)
```bash
cd backend
```
(Create a new sessions called daemon. This will allow the daemon to keep running when you log out of the server)    
```bash
tmux new -s daemon
```
(Give permission to your new user account to run the talosus daemon)
```bash
chmod +x talosusd
```
(Run talosusd)
```bash
./talosusd 
```    
(Then detach from the tmux session)    
```bash
Ctrl + B then D
```
(Create a second tmux session to run the talosus-wallet-rpc in)
```bash
tmux new -s walletRPC
```
(First we need to create a pool wallet for recieveing and processing payments to miners)
(Give permission to your new user account to run the talosus wallet cli)
```bash
chmod +x talosus-wallet-cli   
```
(Create a wallet, name it whatever you want, and give it a password. Remember these for the next step. Copy the wallet address somewhere you will need it later)
(It is recommended to backup the pool wallet. Either copy the .keys file somewhere safe or copy the mnemonic the wallet generates. You can seed this by typing "seed" and pressing Enter)
```bash
./talosus-wallet-cli 
```   
(Close the CLI wallet)
```bash
Ctrl + C 
```
(Give permission to your new user account to run the talosus wallet rpc)
```bash
chmod +x talosus-wallet-rpc
```
(Run the rpc wallet. You will need to replace <WalletName>and <WalletPassword> with the ones you created in the previous step)
```bash
./talosus-wallet-rpc --wallet-file <WalletName> --rpc-bind-port 19892 --disable-rpc-login --password <WalletPassword> --daemon-address 127.0.0.1:38883 --trusted-daemon
```
(Then detach from the tmux session)    
```bash
Ctrl + B then D
```
(Move back to the talosus_pool directory)
```bash    
cd ..
```
(Move to the pool directory)    
```bash
cd pool
```
(Edit the config file for your server. You will need the wallet address from earlier and the IP of your server)
```bash
nano config.json 
```
(At the top where it says "poolHost": "POOLHOST" replace POOLHOST with your servers IP) - **Required**
    
(At line 30 where it says "poolAddress": "WALLETADDRESS" replace WALLETADDRESS with the wallet address you created earlier) - **Required**

(At line 139 where it says "password": "ADMINPASSWORD" replace ADMINPASSWORD with whatever you want. This will be use to access the admin pages of the pool) - **Required**

(You can change the pool fee on line 126 "poolFee": 1, change 1 to whatever you want. 1% by default) - Optional

(Exit nano)
```bash
Ctrl + X 
```    
(Save the changes)
```bash
Y then Enter 
``` 

(Create a third tmux session to run the pool backend in)
```bash
tmux new -s poolBackend
```    
(Install the modules and their dependencies requested from the packages.json. This may take a while)
```bash
npm update
```
(Run the pool. Hopefully you won't see any errors)
```bash
node init.js
```
(Then detach from the tmux session)    
```bash
Ctrl + B then D
```
(Swap back to the root account. Give the root password when asked)
```bash
su -
```
(Install the apache server to allow us to show the frontend interface)
```bash     
apt-get install apache2  
```
(Move to the html folder. Anything in here can be viewed by anyone who visits your domain so be careful)
```bash
cd /var/www/html
```
(Delete the placeholder index.html)
```bash
rm index.html
```
(Download the pool frontend files. Make sure you don't miss the period at the end)
```bash
git clone https://Wasntafairfight@bitbucket.org/talosus/talosus_pool_frontend.git .
```
(Edit the config.js for your server)
```bash
nano config.js
```
(At the top where it says var api = "http://poolhost:8117"; you will need to change "poolhost" to your servers IP)

(You can edit the URLs of the email, Telegram, Discord for your pool or leave them as the default ones)

(Exit nano)
```bash
Ctrl + X 
```    
(Save the changes)
```bash
Y then Enter 
``` 

Tips
===

If you want to check on a tmux session then type;
```bash
tmux attach -t <sessionname>
```
<sessionname> is one of three; 
-poolBackend
-daemon
-walletRPC
Dont forget to detatch from the session when you are done by pressing Ctrl + B then D.
    
To log into the admin panel visit your pool in your browser and add "admin.html" to the url. For example www.talosuspool.com becomes www.talosuspool.com/admin.html. You will then be asked to enter the password you set earlier in the config file.


It is always advisable to secure your server. Sadly there are too many different setups to provide a meaniful tutorial on securing your pool server.

Googling 

"Ubuntu 16.04 Hardening" 

and 

"Apache2 Hardening" 

Should get you going in the right direction.

Otherwise the pool should now be running and allowing connections.
    
If you have any issues please feel free to ask in the support channels.
    
Pools
===

  www.talosuspool.com - Official Pool 1% fee 

Donations
---------

Thanks for supporting my works on this project! If you want to make a donation to [Daniel Vandal](https://github.com/dvandal/), the developer of this project, you can send any amount of your choice to one of theses addresses:

* Bitcoin (BTC): `17XRyHm2gWAj2yfbyQgqxm25JGhvjYmQjm`
* Bitcoin Cash (BCH): `qpl0gr8u3yu7z4nzep955fqy3w8m6w769sec08u3dp`
* Ethereum (ETH): `0x83ECF65934690D132663F10a2088a550cA201353`
* Litecoin (LTC): `LS9To9u2C95VPHKauRMEN5BLatC8C1k4F1`
* Monero (XMR): `49WyMy9Q351C59dT913ieEgqWjaN12dWM5aYqJxSTZCZZj1La5twZtC3DyfUsmVD3tj2Zud7m6kqTVDauRz53FqA9zphHaj`
* Graft (GRFT): `GBqRuitSoU3PFPBAkXMEnLdBRWXH4iDSD6RDxnQiEFjVJhWUi1UuqfV5EzosmaXgpPGE6JJQjMYhZZgWY8EJQn8jQTsuTit`
* Haven (XHV): `hvxy2RAzE7NfXPLE3AmsuRaZztGDYckCJ14XMoWa6BUqGrGYicLCcjDEjhjGAQaAvHYGgPD7cGUwcYP7nEUs8u6w3uaap9UZTf`
* IntenseCoin (ITNS): `iz4fRGV8XsRepDtnK8XQDpHc3TbtciQWQ5Z9285qihDkCAvB9VX1yKt6qUCY6sp2TCC252SQLHrjmeLuoXsv4aF42YZtnZQ53`
* Masari (MSR): `5n7mffxVT9USrq7tcG3TM8HL5yAz7MirUWypXXJfHrNfTcjNtDouLAAGex8s8htu4vBpmMXFzay8KG3jYGMFhYPr2aMbN6i`
* Stellite (XTL): `Se45GzgpFG3CnvYNwEFnxiRHD2x7YzRnhFLdxjUqXdbv3ysNbfW5U7aUdn87RgMRPM7xwN6CTbXNc7nL5QUgcww11bDeypTe1`

These donations go to the author of this pool. All we did was change the tutorial and a few settings. Daniel Vandal deserves all the credit and donations you can give.

Credits
---------

* [fancoder](//github.com/fancoder) - Developer on cryptonote-universal-pool project from which current project is forked.
* [Daniel Vandal](//github.com/dvandal/) - Developer of pool software we customised.

License
-------
Released under the GNU General Public License v2

http://www.gnu.org/licenses/gpl-2.0.html
